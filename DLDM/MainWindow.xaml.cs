﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Drawing;
using System.IO;


namespace DLDM
{
    /// <summary>
    /// MainWindow.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class MainWindow : Window
    {
        String string_source_path = "";
        String string_output_path = AppDomain.CurrentDomain.BaseDirectory;
        String string_savefile_name = "picture";
        String before_file_name = "";
        String before_outfile_name = "";
        Queue<String> queue_file = new Queue<string>();
        Image image;
        BitmapImage bi;
        int mode_num_1 = 0, mode_num_2 = 0, mode_num_3 = 0, mode_num_4 = 0;
 
        public MainWindow()
        {
            InitializeComponent();
            InitUI();
        }

        
        public void InitUI()
        {
            tb_main_debug.Text = "Start DLDM\n";
            tb_sub_debug.Text = "";
            tb_command.Text = "";
            tb_sp_d.Text = "";
            tb_op_d.Text = string_output_path;
            tb_sfn_d.Text = string_savefile_name;
            tb_info_degug.Text = "Input Num0 or 5: Pass Picture \n"
                               + "Input Num1 or 1: 1 Body Picture(Saved OutputFolder/body \r\n"
                               + "Input Num2 or 2: 2 More Bodys Picture(Saved OutputFolder/bodys \r\n"
                               + "Input Num3 or 3: 1 Upper Body Picture(Saved OutputFolder/upper_body \r\n"
                               + "Input Num4 or 4: 2 More Upper Bodys Picture(Saved OutputFolder/upper_bodys \r\n";
        }

        private void Button_SourcePath_Click(object sender, RoutedEventArgs e)
        {
            BrowseForFolderDialog dlg = new BrowseForFolderDialog();
            dlg.Title = "Select a folder and click OK!";
            dlg.InitialExpandedFolder = @"c:\";
            dlg.OKButtonText = "OK!";
            if (true == dlg.ShowDialog(this))
            {
                //MessageBox.Show(dlg.SelectedFolder, "Selected Folder");
                string_source_path = ""+dlg.SelectedFolder;
                tb_sp_d.Text = string_source_path;
            }
             
        }

        private void Button_OutPath_Click(object sender, RoutedEventArgs e)
        {
            BrowseForFolderDialog dlg = new BrowseForFolderDialog();
            dlg.Title = "Select a folder and click OK!";
            dlg.InitialExpandedFolder = @"c:\";
            dlg.OKButtonText = "OK!";
            if (true == dlg.ShowDialog(this))
            {
                //MessageBox.Show(dlg.SelectedFolder, "Selected Folder");
                string_output_path = "" + dlg.SelectedFolder;
                tb_op_d.Text = string_output_path;
            }
        }

        private void Button_FileName_Click(object sender, RoutedEventArgs e)
        {
            string_savefile_name = tb_sfn_d.Text;
            MainDebug_Set("Set Save File Name: "+string_savefile_name+"\n");
        }

        private void Button_DataLoad_Click(object sender, RoutedEventArgs e)
        {
            if(System.IO.Directory.Exists(string_source_path))
            {
                System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(string_source_path);
                String string_directoryinfo = "";
                foreach (var item in di.GetFiles())
                {
                    String msg = "" + item.Name;
                    String[] s_image = msg.Split('.');
                    if(s_image.Length>1)
                    {
                        if (s_image[1].Equals("bmp") || s_image[1].Equals("jpg") || s_image[1].Equals("png") || s_image[1].Equals("gif"))
                        {
                            string_directoryinfo += msg + "\r\n";
                            queue_file.Enqueue(msg);
                        }
                        
                    }
                    
                }
                SetImage();
                
                tb_sub_debug.Text = "Find " + queue_file.Count+"EA File";
                MainDebug_Set(string_directoryinfo);
            }
        }

        public void SetImage()
        {
            if(queue_file.Count>0)
                {
                    bi = new BitmapImage();
                    bi.BeginInit();
                    before_file_name = queue_file.Dequeue();
                    bi.UriSource = new Uri(@"" + string_source_path + "\\" + before_file_name);
                    bi.EndInit();

                    imagev.Source = bi;
                }
            else
            {
                MessageBox.Show("Empty");
            }
        }
        private void Input_Command(object sender, KeyEventArgs e)
        {
            Boolean com_f = true;
            switch(e.Key)
            {
                case Key.NumPad1:
                case Key.D1:
                    MainDebug_Set("1\n");                    
                    Save_Output_File(mode_num_1++, 1);
                    break;
                case Key.NumPad2:
                case Key.D2:
                    MainDebug_Set("2\n");                    
                    Save_Output_File(mode_num_2++, 2);
                    break;
                case Key.NumPad3:
                case Key.D3:
                    MainDebug_Set("3\n");                    
                    Save_Output_File(mode_num_3++, 3);
                    break;
                case Key.NumPad4:
                case Key.D4:
                    MainDebug_Set("4\n");                    
                    Save_Output_File(mode_num_4++, 4);
                    break;
                case Key.NumPad0:
                case Key.D5:
                    MainDebug_Set("0\n");
                    break;    
                default:
                    MessageBox.Show("Number Pad 1~4");
                    com_f = false;
                    break;
            }
            tb_command.Text = "";
            tb_sub_debug.Text = "Remain " + queue_file.Count + "EA File";
            if(com_f) SetImage();
            
        }

        public void Save_Output_File(int num, int mode)
        {
            String s_mode ="";
            String s_mode_d = "";
            String s_mode_f = "";
            if(mode==1) s_mode = "body";
            else if (mode == 2) s_mode = "bodys";
            else if (mode == 3) s_mode = "upper_body";
            else if (mode == 4) s_mode = "upper_bodys";
            s_mode_d = string_output_path + "\\" + s_mode;
            s_mode_f = string_savefile_name + "_" + s_mode + "_" + num + ".jpg";
            DirectoryInfo di = new DirectoryInfo(s_mode_d);
            if (di.Exists == false)
            {
                di.Create();
            }

            BitmapEncoder encoder = null;
            // 파일 생성
            FileStream stream = new FileStream(s_mode_d+"\\"+s_mode_f, FileMode.Create, FileAccess.Write);
            encoder = new JpegBitmapEncoder();
            // 인코더 프레임에 이미지 추가
            encoder.Frames.Add(BitmapFrame.Create(bi));
            // 파일에 저장
            encoder.Save(stream);
            stream.Close();
            before_file_name = s_mode_d + "\\" + s_mode_f;
            MainDebug_Set("File Save: "+ before_file_name+"\n");
        
        }
        
        public void MainDebug_Set(String msg)
        {
            tb_main_debug.AppendText(msg);
            //tb_main_debug.SelectionStart = tb_main_debug.Text.Length;//맨 마지막 선택...
            tb_main_debug.ScrollToEnd();
        }
   }

}
